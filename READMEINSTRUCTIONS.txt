----------------INSTRUCTIONS TO RUN WEBOTS SIMULATION-----------------------------
1. Install Webots on your local machine from this link: https://cyberbotics.com/#cyberbotics
2. Download this unzip the file into a desired directory
3. Open the webots simulator folder
4. Open the following directory "\webots simulator\robot_ant\worlds"
5. Right-click on the army_ant_bridge.wbt file
6. Select Open With...
7. Choose Webots
