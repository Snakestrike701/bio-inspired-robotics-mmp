"""worker_ant_controller controller."""
# Author: Kanen Kuruvilla [kak59]

# You may need to import some classes of the controller module. Ex:
#  from controller import Robot, Motor, DistanceSensor
from controller import Robot, Connector, DistanceSensor, Motor

# create the Robot instance.
robot = Robot()

# get the time step of the current world.
timestep = 64

# Initialise custom data to empty
robot.setCustomData('')

# Initialise distance sensors
topDistanceSensor = robot.getDevice('topDSensor')
topDistanceSensor.enable(timestep)
bottomDistanceSensor = robot.getDevice('bottomDSensor')
bottomDistanceSensor.enable(timestep)

# Initialise propeller motors
MAINHELIXVELOCITY = 100.0
topPropeller = robot.getDevice('topRotor')
topPropeller.setPosition(float('inf'))
topPropeller.setVelocity(0.0)
bottomPropeller = robot.getDevice('bottomRotor')
bottomPropeller.setPosition(float('inf'))
bottomPropeller.setVelocity(0.0)

# Initialise all connector node
frontConnect = robot.getDevice('frontConnector')
frontConnect.enablePresence(timestep)
backConnect = robot.getDevice('backConnector')
backConnect.enablePresence(timestep)

# Main loop:
# - perform simulation steps until Webots is stopping the controller
while robot.step(timestep) != -1:
       
    # If worker ant robot in front of another ant robot, connect to each other
    if (frontConnect.getPresence() == 1):
        frontConnect.lock()
    elif (backConnect.getPresence() == 1):
        backConnect.lock()
        
    # If both front and back sides are magnetically connected, the robot is ready to propel
    if (frontConnect.isLocked()):
        robot.setCustomData('readyToFly: True')
    else:
        robot.setCustomData('readyToFly: False')
    
    # Check if custom data status changed by supervisor to allow permission to fly
    if (robot.getCustomData() == ('PermissionToFly')):
        # see which side of the propeller should activate
        print('reached here')
        
    
# Enter here exit cleanup code.