"""lead_ant_controller controller."""
# Author: Kanen Kuruvilla [kak59]

# You may need to import some classes of the controller module. Ex:
#  from controller import Robot, Motor, DistanceSensor
from controller import Robot, DistanceSensor, Motor, Connector

# create the Robot instance.
robot = Robot()

# get the time step of the current world.
timestep = 64

# You should insert a getDevice-like function in order to get the
# instance of a device of the robot. Something like:
#  motor = robot.getDevice('motorname')
#  ds = robot.getDevice('dsname')
#  ds.enable(timestep)

# Initialise custom data to empty
robot.setCustomData('')

# Initialise distance sensors
distanceSensorList = ['frontLeftSensor', 
                      'frontRightSensor', 
                      'cornerLeftSensor', 
                      'cornerRightSensor']
distanceSensors = dict(zip(distanceSensorList, [None]*len(distanceSensorList)))
for dsName in distanceSensors.keys():
    distanceSensors[dsName] = robot.getDevice(dsName)
    distanceSensors[dsName].enable(timestep)
    
# Initialise wheel motors
wheelsNameList = ['FrontLeftWheel', 
                  'FrontRightWheel', 
                  'BackLeftWheel',
                  'BackRightWheel']
wheelsDict = dict(zip(wheelsNameList, [None]*len(wheelsNameList)))
for wheel in wheelsDict.keys():
    wheelsDict[wheel] = robot.getDevice(wheel)
    wheelsDict[wheel].setPosition(float('inf'))
    wheelsDict[wheel].setVelocity(0.0)
    
# Initialise front slider, rotational motor and position sensors
frontSliderMotor = robot.getDevice('sliderMotor')
frontSliderMotor.setPosition(0.0)
frontSliderPosition = robot.getDevice('linearPosition')
frontRotationalMotor = robot.getDevice('rotationalMotor')
frontRotationalMotor.setPosition(0.0)
frontRotationalPosition = robot.getDevice('hingePosition')

# Initialise connector nodes
frontConnect = robot.getDevice('frontConnector')
frontConnect.enablePresence(timestep)
frontSliderPosition.enable(timestep)
frontRotationalPosition.enable(timestep)
backConnect = robot.getDevice('backConnector')
backConnect.enablePresence(timestep)

#--------------------Functions-----------------------
# Reset the position of any motor
def resetPosition(motor):
    motor.setPosition(0.0)

# Gets the average front left and right five sensor values, compares each left and right average value and corrects the robot orientation once the robot has detected a bridge gap
frontDistanceSensorVals = {'left': [], 'right': []}
selfAligned = False
def selfAlignBridgeGap(dsDict, wheelsDict, maxSpeed):
    global frontDistanceSensorVals, selfAligned
    if (len(frontDistanceSensorVals['left']) == 5) and (len(frontDistanceSensorVals['right']) == 5):
        averageFLDSValue = sum(frontDistanceSensorVals['left'])/float(5.0)
        averageFRDSValue = sum(frontDistanceSensorVals['right'])/float(5.0)
        if (averageFLDSValue - averageFRDSValue) > float(300.0):
            print('TURN LEFT')
            # turn left
            wheelsDict['FrontLeftWheel'].setVelocity(-maxSpeed)
            wheelsDict['FrontRightWheel'].setVelocity(maxSpeed)
            wheelsDict['BackLeftWheel'].setVelocity(-maxSpeed)
            wheelsDict['BackRightWheel'].setVelocity(maxSpeed)
        elif (averageFRDSValue - averageFLDSValue) > float(300.0):
            print('TURN RIGHT')
            # turn right
            wheelsDict['FrontLeftWheel'].setVelocity(maxSpeed)
            wheelsDict['FrontRightWheel'].setVelocity(-maxSpeed)
            wheelsDict['BackLeftWheel'].setVelocity(maxSpeed)
            wheelsDict['BackRightWheel'].setVelocity(-maxSpeed)
        elif (int(abs(averageFRDSValue-averageFLDSValue)) in range(0,31)):
            # Stop the robot
            wheelsDict['FrontLeftWheel'].setVelocity(0.0)
            wheelsDict['FrontRightWheel'].setVelocity(0.0)
            wheelsDict['BackLeftWheel'].setVelocity(0.0)
            wheelsDict['BackRightWheel'].setVelocity(0.0)
            selfAligned = True
        frontDistanceSensorVals['left'].clear()
        frontDistanceSensorVals['right'].clear()
    else:
        frontDistanceSensorVals['left'].append(dsDict['frontLeftSensor'].getValue())
        frontDistanceSensorVals['right'].append(dsDict['frontRightSensor'].getValue())
    
# Rotate the front magnetic connector joint to connect to the ground once touched and set connectedToGround state to true
sliderPositionRange = {'min': -0.005, 'max': 0.005}
rotationPositionRange = {'min': -1.57, 'max': 1.57}
sliderFinished = False
rotationFinished = False
def connectToGround(frontSlider, frontRotation, frontConnector):
    global sliderPositionRange, rotationPositionRange, frontSliderPosition, frontRotationalPosition, sliderFinished, rotationFinished
    frontRotation.setPosition(rotationPositionRange['min'])
    if (frontRotationalPosition.getValue() < (rotationPositionRange['min']+0.07)):
        rotationFinished = True
        frontSlider.setPosition(sliderPositionRange['min'])
    if (frontSliderPosition.getValue() < (sliderPositionRange['min']+0.001)):
        sliderFinished = True
    if ((sliderFinished) and (rotationFinished) and (frontConnector.getPresence() == 1)):
        frontConnector.lock()
    
maxSpeed = 1.0
mainLoopCount = 0
bridgeGapDetected = False
previousAverageFrontDistance = float(0.0)
#--------------------------Main loop-----------------------------------:
# - perform simulation steps until Webots is stopping the controller
while robot.step(timestep) != -1:

    # If bridge gap is detected
    if (bridgeGapDetected): 
        # Self correct the orientation of the robot 
        if not(selfAligned):
           selfAlignBridgeGap(distanceSensors, wheelsDict, maxSpeed)
    else:
        # Otherwise go straight
        wheelsDict['FrontLeftWheel'].setVelocity(maxSpeed)
        wheelsDict['FrontRightWheel'].setVelocity(maxSpeed)
        wheelsDict['BackLeftWheel'].setVelocity(maxSpeed)
        wheelsDict['BackRightWheel'].setVelocity(maxSpeed)
        
    # Gets the average distance of both front sensors and detects whether there is a bridge gap
    if (mainLoopCount == 0):
        currentAverageFrontDistance = (distanceSensors['frontLeftSensor'].getValue() + distanceSensors['frontRightSensor'].getValue())/float(2.0)
        previousAverageFrontDistance = currentAverageFrontDistance
    else:
        currentAverageFrontDistance = (distanceSensors['frontLeftSensor'].getValue() + distanceSensors['frontRightSensor'].getValue())/float(2.0)
        if (currentAverageFrontDistance - previousAverageFrontDistance) > float(500.0):
            print('BRIDGE GAP DETECTED!')
            bridgeGapDetected = True
        previousAverageFrontDistance = currentAverageFrontDistance
    
    # Once self aligned, connect the front magnet to the ground
    if not(frontConnect.isLocked()) and (selfAligned):
        connectToGround(frontSliderMotor, frontRotationalMotor, frontConnect)
        
    #If front magnet is locked to the ground change the robot status and check if back connector gets presence
    if ((frontConnect.isLocked()) and (selfAligned)):
        if (robot.getCustomData() == ''):
            robot.setCustomData('connectedToGround: ' + str(True))
        
        # If back connector detects presence of another robot, connect
        if (backConnect.getPresence() == 1):
            backConnect.lock()
    
    
    # Enter here functions to send actuator commands, like:
    #  motor.setPosition(10.0)
    mainLoopCount += 1

# Enter here exit cleanup code.
