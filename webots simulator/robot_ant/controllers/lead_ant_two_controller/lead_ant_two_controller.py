"""lead_ant_two_controller controller."""
# Author: Kanen Kuruvilla [kak59]

# You may need to import some classes of the controller module. Ex:
#  from controller import Robot, Motor, DistanceSensor
from controller import Robot, DistanceSensor, Motor, Connector

# create the Robot instance.
robot = Robot()

# get the time step of the current world.
timestep = 64

# You should insert a getDevice-like function in order to get the
# instance of a device of the robot. Something like:
#  motor = robot.getDevice('motorname')
#  ds = robot.getDevice('dsname')
#  ds.enable(timestep)

# Initialise custom data to empty
robot.setCustomData('')

# Initialise distance sensors
distanceSensorList = ['frontLeftSensor2', 
                      'frontRightSensor2', 
                      'cornerLeftSensor2', 
                      'cornerRightSensor2']
distanceSensors = dict(zip(distanceSensorList, [None]*len(distanceSensorList)))
for dsName in distanceSensors.keys():
    distanceSensors[dsName] = robot.getDevice(dsName)
    distanceSensors[dsName].enable(timestep)
    
# Initialise wheel motors
wheelsNameList = ['FrontLeftWheel2', 
                  'FrontRightWheel2', 
                  'BackLeftWheel2',
                  'BackRightWheel2']
wheelsDict = dict(zip(wheelsNameList, [None]*len(wheelsNameList)))
for wheel in wheelsDict.keys():
    wheelsDict[wheel] = robot.getDevice(wheel)
    wheelsDict[wheel].setPosition(float('inf'))
    wheelsDict[wheel].setVelocity(0.0)
    
# Initialise front slider, rotational motor and position sensors
frontSliderMotor = robot.getDevice('sliderMotor2')
frontSliderMotor.setPosition(0.0)
frontSliderPosition = robot.getDevice('linearPosition2')
frontRotationalMotor = robot.getDevice('rotationalMotor2')
frontRotationalMotor.setPosition(0.0)
frontRotationalPosition = robot.getDevice('hingePosition2')

# Initialise connector nodes
frontConnect = robot.getDevice('frontConnector2')
frontConnect.enablePresence(timestep)
frontSliderPosition.enable(timestep)
frontRotationalPosition.enable(timestep)
backConnect = robot.getDevice('backConnector2')
backConnect.enablePresence(timestep)

#--------------------Functions-----------------------
# Reset the position of any motor
def resetPosition(motor):
    motor.setPosition(0.0)

# Rotate the front magnetic connector joint to connect to the ground once touched and set connectedToGround state to true
sliderPositionRange = {'min': -0.005, 'max': 0.005}
rotationPositionRange = {'min': -1.57, 'max': 1.57}
sliderFinished = False
rotationFinished = False
def connectToGround(frontSlider, frontRotation, frontConnector):
    global sliderPositionRange, rotationPositionRange, frontSliderPosition, frontRotationalPosition, sliderFinished, rotationFinished
    frontRotation.setPosition(rotationPositionRange['min'])
    if (frontRotationalPosition.getValue() < (rotationPositionRange['min']+0.07)):
        rotationFinished = True
        frontSlider.setPosition(sliderPositionRange['min'])
    if (frontSliderPosition.getValue() < (sliderPositionRange['min']+0.001)):
        sliderFinished = True
    if ((sliderFinished) and (rotationFinished) and (frontConnector.getPresence() == 1)):
        frontConnector.lock()
    
maxSpeed = 1.0
mainLoopCount = 0
#--------------------------Main loop-----------------------------------:
# - perform simulation steps until Webots is stopping the controller
while robot.step(timestep) != -1:    
    # Once self aligned, connect the front magnet to the ground
    if not(frontConnect.isLocked()) and (selfAligned):
        connectToGround(frontSliderMotor, frontRotationalMotor, frontConnect)
        if (robot.getCustomData() == ''):
            robot.setCustomData('connectedToGround: ' + str(True))
        
    
    
    # Enter here functions to send actuator commands, like:
    #  motor.setPosition(10.0)
    mainLoopCount += 1

# Enter here exit cleanup code.
