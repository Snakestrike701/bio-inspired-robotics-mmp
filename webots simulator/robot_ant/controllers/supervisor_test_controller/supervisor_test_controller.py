# Author: Kanen Kuruvilla [kak59]
"""supervisor_controller controller."""

# You may need to import some classes of the controller module. Ex:
#  from controller import Robot, Motor, DistanceSensor
from controller import Supervisor

# create the Robot instance.
robot = Supervisor()

# get the time step of the current world.
timestep = 64

# Get all lead ant nodes and fields
leadAntRobotNode = robot.getFromDef('LEADANT')
leadAntTranslationField = leadAntRobotNode.getField('translation')
leadAntRotationField = leadAntRobotNode.getField('rotation')
leadAntFrontMagnetNode = robot.getFromDef('FRONTMAGNET')

# Get all worker ant nodes and fields
numberOfWorkerAnts = 1
workerAntTranslationDict = {}
workerAntRotationDict = {}
for i in range(numberOfWorkerAnts):
    workerAntNode = robot.getFromDef('WORKERANT' + str(i+1))
    workerAntTranslationDict['WORKERANT' + str(i+1)] = workerAntNode.getField('translation')
    workerAntRotationDict['WORKERANT' + str(i+1)] = workerAntNode.getField('rotation')

# Get platform one nodes and fields
platformOneNode = robot.getFromDef('PLATFORM_ONE')
platformOneTranslationField = platformOneNode.getField('translation')
connectorOneNode = robot.getFromDef('CONNECTORONE')
connectorOneTranslationField = connectorOneNode.getField('translation')

# Get platform two nodes and fields
platformTwoNode = robot.getFromDef('PLATFORM_TWO')
platformTwoTranslationField = platformTwoNode.getField('translation')
connectorTwoNode = robot.getFromDef('CONNECTORTWO')
connectorTwoTranslationField = connectorTwoNode.getField('translation')

#-----------------Functions--------------------
#Returns the new coordinate by adding a vector to the first original point
def newVectorCoordinate(coordinateToMoveTo, currentCoordinate):
    vector = [(coordinateToMoveTo[0] - currentCoordinate[0]), (coordinateToMoveTo[1] - currentCoordinate[1])]
    return [currentCoordinate[0] + vector[0], currentCoordinate[1] + vector[1]]
    
# Main loop:
# - perform simulation steps until Webots is stopping the controller
while robot.step(timestep) != -1:
    # Track lead ant translation and rotation coordinate
    currentLeadAntTranslation = leadAntTranslationField.getSFVec3f()
    currentLeadAntRotation = leadAntRotationField.getSFRotation()
    
    # If the robot is still, align both platform and robot connectors
    if ((leadAntFrontMagnetNode.getVelocity()[0] < 0.0) and (leadAntFrontMagnetNode.getVelocity()[1] < 0.0)):
       # Track lead ant front connector coordinate
       currentLeadAntRobotConnectorCoordinate = [currentLeadAntTranslation[0] + 0.04, currentLeadAntTranslation[1]]         
       
       # Get the platform one connector coordinate
       currentPlatformOneTranslation = platformOneTranslationField.getSFVec3f()
       currentConnectorOneTranslation = connectorOneTranslationField.getSFVec3f()
       currentPlatformOneConnectorCoordinate = [currentPlatformOneTranslation[0] + currentConnectorOneTranslation[0], currentPlatformOneTranslation[1] + currentConnectorOneTranslation[1]]
       
       # Get the vector the platform vector has to move to
       newPlatformOneConnectorCoordinate = newVectorCoordinate(currentLeadAntRobotConnectorCoordinate, currentPlatformOneConnectorCoordinate)
       newPlatformOneConnectorCoordinate = [newPlatformOneConnectorCoordinate[0] - currentPlatformOneTranslation[0], newPlatformOneConnectorCoordinate[1] - currentPlatformOneTranslation[1], currentConnectorOneTranslation[2]]
    
       # Set the new vector to the CONNECTORONE node translation field
       connectorOneTranslationField.setSFVec3f(newPlatformOneConnectorCoordinate)
    
    # If the lead ant robot is connected to the ground, translate all ant robots behind to form the bridge
    if (leadAntRobotNode.getField('customData').getSFString() == ('connectedToGround: ' + str(True))):
        # For every worker ant, translate it to be roughly behind each other
        previousAntXTranslation = currentLeadAntTranslation[0]
        for i in range(numberOfWorkerAnts):
            
            workerAntTranslationDict['WORKERANT' + str(i+1)].setSFVec3f([previousAntXTranslation-0.09, workerAntTranslationDict['WORKERANT' + str(i+1)].getSFVec3f()[1], 0.265])
            workerAntRotationDict['WORKERANT' + str(i+1)].setSFRotation([currentLeadAntRotation[0], currentLeadAntRotation[1], currentLeadAntRotation[2], currentLeadAntRotation[3]])
            previousAntXTranslation = workerAntTranslationDict['WORKERANT' + str(i+1)].getSFVec3f()[0]
            
            # Set custom data status for every worker robot ant
            workerAntNode = robot.getFromDef('WORKERANT' + str(i+1))
            workerAntNode.getField('customData').setSFString('leadAntConnectedToGround: ' + str(True))
        
        
    # Check whether custom data for all worker ant robots are ready to fly
    allReady = []
    for i in range(numberOfWorkerAnts):
        workerAntNode = robot.getFromDef('WORKERANT' + str(i+1))
        if (workerAntNode.getField('customData').getSFString() == ('readyToFly: True')):
            allReady.append(True)
        else:
            allReady.append(False)
    
    # If all worker ants are ready to fly, set new custom data to all worker ants to give permission to fly       
    if (all(allReady)):
        for i in range(numberOfWorkerAnts):
            workerAntNode = robot.getFromDef('WORKERANT' + str(i+1))
            workerAntNode.getField('customData').setSFString('PermissionToFly')  
    

# Enter here exit cleanup code.
